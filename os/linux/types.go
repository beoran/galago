package linux 

// suseconds_t varies by platform, 32 on 32 bits, 64 on 64 bits
type Suseconds_t = Long
// time_t is aways 64 bits signed, even on 32 bits platforms.
type Time_t = int64

// A time value that is accurate to the nearest
// microsecond but also has a range of years.
type Timeval struct {
    Tv_sec Time_t	// Seconds.
    Tv_usec Suseconds_t  // Microseconds.
}



