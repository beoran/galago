package input

import "testing"
import "time"
// import "os"
// import "syscall"
import "fmt"
import "gitlab.com/beoran/galago/os/linux"



const IN1 = "by-id/usb-0583_USB_2-axis_8-button_gamepad-event-joystick"
const IN2 = "by-id/usb-PixArt_USB_Optical_Mouse-event-mouse"
const IN3 = "by-id/usb-Logitech_USB_Receiver-if02-event-mouse"
const IN = IN3

func TestList(t * testing.T) {
    names, err := List()
    if err != nil {
        t.Errorf("Error List: %s\n", err)
        return
    }
    for _, name := range names {
        t.Logf("Device: %s\n", name)
    }
}


func TestGetDriverVersion(t * testing.T) {
    device , err := Open(IN)
    if err != nil {
        t.Errorf("Error Open: %s\n", err)
        return
    }
    defer device.Close()
    version, err := device.DriverVersion()
    if err != nil {
        t.Errorf("Error GetDriverVersion: %s (%s %x)\n", err, 
                    "EVIOCGVERSION", linux.EVIOCGVERSION)
        return
    }
    if version != linux.EV_VERSION {
        t.Errorf("Version %x should be %x", version, linux.EV_VERSION)
    }
    t.Logf("GetDriverVersion: %d", version)
}


func TestGetName(t * testing.T) {
    device , err := Open(IN)
    if err != nil {
        t.Errorf("Error Open: %s\n", err)
        return
    }
    defer device.Close()
    name, err := device.Name()
    if err != nil {
        t.Errorf("Error GetName: %s\n", err)
        return
    }
    t.Logf("GetName: %s", name)
}


func TestGetTopology(t * testing.T) {
    device , err := Open(IN)
    if err != nil {
        t.Errorf("Error Open: %s\n", err)
        return
    }
    defer device.Close()
    name, err := device.Topology()
    if err != nil {
        t.Errorf("Error Topology: %s\n", err)
        return
    }
    t.Logf("Topology: %s", name)
}


func TestGetId(t * testing.T) {
    device , err := Open(IN)
    if err != nil {
        t.Errorf("Error Open: %s\n", err)
        return
    }
    defer device.Close()
    id, err := device.Id()
    if err != nil {
        t.Errorf("Error GetId: %s\n", err)
        return
    }
    t.Logf("GetId: bustype 0x%x, vendor 0x%x, product 0x%x, version 0x%x", 
        id.Bustype, id.Vendor, id.Product, id.Version)
}

func TestSupportedEvents(t * testing.T) {
    device , err := Open(IN)
    if err != nil {
        t.Errorf("Error Open: %s\n", err)
        return
    }
    defer device.Close()
    events, err := device.SupportedEvents()

	t.Logf("Supported events:\n")
	for i, ev := range events {
        t.Logf("Supported: %d: %d %s", i, int(ev), ev.Name())
    }
		
}

func TestSupportedKeys(t * testing.T) {
    device , err := Open(IN)
    if err != nil {
        t.Errorf("Error Open: %s\n", err)
        return
    }
    defer device.Close()
    keys, err := device.SupportedKeys()

	t.Logf("Supported keys:\n")
	for i, key := range keys {
        t.Logf("Supported: %d: %d %s", i, int(key), key.Name())
    }
		
}


func TestSupportedAxes(t * testing.T) {
    device , err := Open(IN)
    if err != nil {
        t.Errorf("Error Open: %s\n", err)
        return
    }
    defer device.Close()
    axes, err := device.SupportedAxes()
    
    t.Logf("Supported axes:\n")
	for i, ax := range axes {
        t.Logf("Supported: %d: %d %s\n", i, uint(ax.Index), ax.String())
    }
}


func TestRelativeAxes(t * testing.T) {
    device , err := Open(IN)
    if err != nil {
        t.Errorf("Error Open: %s\n", err)
        return
    }
    defer device.Close()
    events, err := device.SupportedRelatives()

	t.Logf("Supported relative axes:\n")
	for i, ev := range events {
        t.Logf("Supported: %d: %d %s", i, int(ev), ev.Name())
    }
		
}

func TestReadEvents(t * testing.T) {
    device , err := Open(IN)
    if err != nil {
        t.Errorf("Error Open: %s\n", err)
        return
    }
    defer device.Close()
    events, err := device.ReadEvents()
    if err != nil {
        t.Errorf("Error ReadEvents: %s\n", err)
        return
    }
    
    t.Logf("Events:\n")
	for i, ev := range events {
        t.Logf("Event: %d: %s\n", i, ev.String())
    }
}

func TestReadEventsBlock(t * testing.T) {
    device , err := Open(IN)
    if err != nil {
        t.Errorf("Error Open: %s\n", err)
        return
    }
    defer device.Close()
    events, err := device.ReadEventsBlock()
    if err != nil {
        t.Errorf("Error ReadEvents: %s\n", err)
        return
    }
    
    t.Logf("Events:\n")
	for i, ev := range events {
        t.Logf("Event: %d: %s\n", i, ev.String())
    }
}


func TestDriver(t * testing.T) {
    driver := NewDriver()
    done := false
    go func() { 
            for !done {
                dev := <- driver.Connect
                fmt.Printf("Connected: %s\n", dev.String())  
            }
        }()
    go func() { 
            for !done {
                dev := <- driver.Disconnect
                fmt.Printf("Disconnected: %s\n", dev.String())  
            }
        }()

    go func() {
            for !done {
                err := <- driver.Errors
                fmt.Printf("Error: %s\n", err)  
            }
        }()
          
    go func() { 
            for !done {
                eve := <- driver.Events
                fmt.Printf("%s\n", eve.String())
                if eve.Type == linux.EV_KEY && (
					eve.Code == linux.BTN_JOYSTICK || eve.Code == linux.BTN_MOUSE) {
                    done = true
                }   
            }
        }()

    driver.Start()
    for ! done {
        time.Sleep(1 * time.Second)
    }
    done = true
}


