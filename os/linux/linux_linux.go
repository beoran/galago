
/* linux is a package that is a Go port of the functionality in 
 * the header directory <linux/XXX> as far as is needed for input event
 * handing. Defines have been makked either to consts 
 * or functions in case they were macros with parameters. 
 * Structs have been ported attempting to have the same size in Go and in C.
 * This package is not documented, but the names are, as far as is possible, 
 * keeping go export rule into consideration, the same as in C. 
 * Please see the Linux kernel documentation for more explanations.
 */
package linux
