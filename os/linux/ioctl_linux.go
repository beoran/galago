package linux

// ioctl for linux 


/* ioctl command encoding: 32 bits total, command in lower 16 bits,
 * size of the parameter structure in the lower 14 bits of the
 * upper 16 bits.
 * Encoding the size of the parameter structure in the ioctl request
 * is useful for catching programs compiled with old versions
 * and to avoid overwriting user space outside the user buffer area.
 * The highest 2 bits are reserved for indicating the ``access mode''.
 * NOTE: This limits the max parameter size to 16kB -1 !
 */

/*
 * The following is for compatibility across the various Linux
 * platforms.  The generic ioctl numbering scheme doesn't really enforce
 * a type_ field.  De facto, however, the top 8 bits of the lower 16
 * bits are indeed used as a type_ field, so we might just as well make
 * this explicit here.  Please be sure to use the decoding macros
 * below from now on.
 */
const IOC_NRBITS = 8
// XXX might be different on some platforms
const IOC_TYPEBITS	= 8
// XXX might be 13 on some platforms
const IOC_SIZEBITS	= 14
// XXX might be 3 on some platforms
const IOC_DIRBITS	= 2


const  IOC_NRMASK	= ((1 << IOC_NRBITS)-1)
const IOC_TYPEMASK	= ((1 << IOC_TYPEBITS)-1)
const IOC_SIZEMASK	= ((1 << IOC_SIZEBITS)-1)
const IOC_DIRMASK	= ((1 << IOC_DIRBITS)-1)

const IOC_NRSHIFT	    = 0
const IOC_TYPESHIFT	    = (IOC_NRSHIFT+IOC_NRBITS)
const IOC_SIZESHIFT	    = (IOC_TYPESHIFT+IOC_TYPEBITS)
const IOC_DIRSHIFT	    = (IOC_SIZESHIFT+IOC_SIZEBITS)

/*
 * Direction bits, which any architecture can choose to override
 * before including this file.
 */

const IOC_NONE	= 0

//XXX is 4 on some platforms
const IOC_WRITE = 1

const  IOC_READ = 2

func IOC(dir uint32, type_ rune, nr uint32, size uintptr) uint32 {
	return (((dir)  << IOC_DIRSHIFT) |
	 ((uint32(type_)) << IOC_TYPESHIFT) |
	 (uint32(nr)   << IOC_NRSHIFT) |
	 (uint32(size) << IOC_SIZESHIFT))
}

/* used to create numbers */
func IO(type_ rune,nr uint32) uint32 { 
    return IOC(IOC_NONE,(type_),(nr),0)
}

func IOR(type_ rune, nr uint32, size uintptr)	uint32 { 
     return IOC(IOC_READ,type_,nr, size)
}

func IOW(type_ rune ,nr uint32,size uintptr) uint32	{ 
    return IOC(IOC_WRITE,(type_),(nr),((size)))
}

func IOWR(type_ rune,nr uint32,size uintptr) uint32 { 
    return IOC(IOC_READ|IOC_WRITE,(type_),(nr),((size)))
}

/* used to decode ioctl numbers.. */
func IOC_DIR(nr uint32) uint32 { 
    return (((nr) >> IOC_DIRSHIFT) & IOC_DIRMASK)
}


func IOC_TYPE(nr uint32) uint32 {		
    return (((nr) >> IOC_TYPESHIFT) & IOC_TYPEMASK)
}


func IOC_NR(nr uint32) uint32 { 
    return (((nr) >> IOC_NRSHIFT) & IOC_NRMASK)
}

func IOC_SIZE(nr uint32) uint32 { 		
    return (((nr) >> IOC_SIZESHIFT) & IOC_SIZEMASK)
}

/* ...and for the drivers/sound files... */

const IOC_IN =		(IOC_WRITE << IOC_DIRSHIFT)
const IOC_OUT =		(IOC_READ << IOC_DIRSHIFT)
const IOCINOUT =	((IOC_WRITE|IOC_READ) << IOC_DIRSHIFT)
const IOCSIZE_MASK =	(IOC_SIZEMASK << IOC_SIZESHIFT)
const IOCSIZE_SHIFT =	(IOC_SIZESHIFT)


