

// +build amd64 arm64 ppc64 ppc64le mips64 mips64le
// +build linux 

package linux

// platform specific type aliases for 64 bits linux platforms

type Long = int64

type UnsignedLong = uint64



