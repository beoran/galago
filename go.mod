module gitlab.com/beoran/galago

go 1.12

require (
	github.com/BurntSushi/xgb v0.0.0
	github.com/andybalholm/c2go v0.0.0-20190812182517-7b628d7f3263 // indirect
	github.com/elliotchance/c2go v0.25.9 // indirect
	github.com/seccomp/libseccomp-golang v0.9.1
	golang.org/x/sys v0.0.0-20190919044723-0c1ff786ef13
)

replace github.com/BurntSushi/xgb v0.0.0 => ../xgb
