#include <stdio.h>
#include <linux/input.h>
#include <linux/ioctl.h>


#define pioc(NAME) printf(#NAME " :%lx\n", (long unsigned int)(NAME))

int main(void) {
    pioc(EVIOCGVERSION);
    pioc(EVIOCGID);
    pioc(EVIOCGNAME(256));
    pioc(EVIOCGBIT(EV_SYN, 3));
    pioc(EVIOCGBIT(EV_SYN, 31));
    pioc(EVIOCGBIT(EV_KEY, 767));
    pioc(EVIOCGBIT(EV_ABS, 767));
    pioc(EVIOCGABS(ABS_X));
    pioc(EVIOCGABS(ABS_Y));
    pioc(EVIOCGBIT(EV_MSC, 767));
    pioc(EVIOCGPROP(248));
    pioc(EVIOCGRAB);
    pioc(_IOC_NONE);
    pioc(_IOC_WRITE);
    pioc(_IOC_READ);
    pioc(_IOC_NRSHIFT);
    pioc(_IOC_TYPESHIFT);
    pioc(_IOC_SIZESHIFT);
    pioc(_IOC_DIRSHIFT);
    pioc(_IOC_NRBITS);
    pioc(_IOC_TYPEBITS);
    pioc(_IOC_SIZEBITS);
    pioc(_IOC_DIRBITS);
   
    return 0;
}
