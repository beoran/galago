package controller

import "testing"
import "os"
import "syscall"
// import "fmt"
import "gitlab.com/beoran/galago/os/linux"



const IN = "/dev/input/by-id/usb-0583_USB_2-axis_8-button_gamepad-event-joystick"

func TestGetDriverVersion(t * testing.T) {
    file , err := os.OpenFile(IN, syscall.O_RDWR, 0666)
    if err != nil {
        t.Errorf("Error OpenFile: %s\n", err)
        return
    }
    defer file.Close()
    version, err := GetDriverVersion(file)
    if err != nil {
        t.Errorf("Error GetDriverVersion: %s (%s %x)\n", err, 
                    "EVIOCGVERSION", linux.EVIOCGVERSION)
        return
    }
    if version != linux.EV_VERSION {
        t.Errorf("Version %x should be %x", version, linux.EV_VERSION)
    }
    t.Logf("GetDriverVersion: %d", version)
}


func TestGetName(t * testing.T) {
    file , err := os.OpenFile(IN, syscall.O_RDWR, 0666)
    if err != nil {
        t.Errorf("Error OpenFile: %s\n", err)
        return
    }
    defer file.Close()
    name, err := GetName(file)
    if err != nil {
        t.Errorf("Error GetName: %s\n", err)
        return
    }
    t.Logf("GetName: %s", name)
}

func TestGetId(t * testing.T) {
    file , err := os.OpenFile(IN, syscall.O_RDWR, 0666)
    if err != nil {
        t.Errorf("Error OpenFile: %s\n", err)
        return
    }
    defer file.Close()
    id, err := GetId(file)
    if err != nil {
        t.Errorf("Error GetId: %s\n", err)
        return
    }
    t.Logf("GetId: bustype 0x%x, vendor 0x%x, product 0x%x, version 0x%x", 
        id.Bustype, id.Vendor, id.Product, id.Version)
}




