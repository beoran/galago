package controller

import "gitlab.com/beoran/galago/os/linux"
import "os"
import "unsafe"
import "syscall"
import "fmt"

func Ioctl(file * os.File, code uint32, pointer unsafe.Pointer) error {
    fmt.Printf("ioctl: %d %d %d\n", uintptr(file.Fd()), uintptr(code), uintptr(pointer))
    _, _, errno := syscall.Syscall(
        syscall.SYS_IOCTL,
        uintptr(file.Fd()),
        uintptr(code),
        uintptr(pointer))
    if (errno != 0) {
        return errno
    }
    return nil
}


func GetDriverVersion(file * os.File) (int32, error) {
    res := int32(0) 
    data := unsafe.Pointer(&res)
    err := Ioctl(file, linux.EVIOCGVERSION, data)
    return res, err
}

func GetName(file * os.File) (string, error) {
    buffer := [256]byte{}
    err := Ioctl(file, linux.EVIOCGNAME(uintptr(len(buffer))), unsafe.Pointer(&buffer))
    return string(buffer[0:len(buffer)]), err
}

func GetId(file * os.File) (linux.INPUT_id, error) {
    var result linux.INPUT_id 
    err := Ioctl(file, linux.EVIOCGID, unsafe.Pointer(&result))
    return result, err
}



